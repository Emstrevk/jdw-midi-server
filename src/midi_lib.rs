pub const NOTE_ON_HEX: u8 = 0x90;
pub const NOTE_OFF_HEX: u8 = 0x80;
pub const ALL_NOTES_OFF: u8 = 0x7B;
pub const CLOCK_SYNC: u8 = 0xF8;

pub fn midi_velocity(source_amp: &f32) -> u8 {
    let ceiling = 127;
    let calc = *source_amp * ceiling as f32;
    if calc > ceiling as f32 {
        ceiling
    } else {
        calc as u8
    }
}

pub fn beats_to_milliseconds(beats: f32, bpm: i32) -> f32 {
    beats * (60.0 / bpm as f32) * 1000.0
}
