
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MIDINotePlayMessage {
    pub target: String,
    pub tone: i32,
    pub sus_ms: f32,
    pub amp: f32,
}
