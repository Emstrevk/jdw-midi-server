#![feature(proc_macro_hygiene, decl_macro)]

use std::sync::{Arc, Mutex};
use std::thread;

use crate::model::MIDINotePlayMessage;
use crate::server::JDWMIDIServer;

pub mod server;
pub mod model;
pub mod midi_lib;

pub fn main() {

    let midi_server = JDWMIDIServer::new();
    let midi_ref = Arc::new(Mutex::new(midi_server));

    JDWMIDIServer::poll_outputs(midi_ref.clone());

    subscriber_loop(midi_ref);
}

fn subscriber_loop(midi_server: Arc<Mutex<JDWMIDIServer>>) {
    let context = zmq::Context::new();
    let socket = context.socket(zmq::SUB).unwrap();
    socket.connect("tcp://localhost:5560").unwrap();
    socket.set_subscribe("JDW.PLAY.".as_bytes());

    loop {
        let msg = socket.recv_msg(0).unwrap();
        let decoded_msg = msg.as_str().unwrap().split("::").collect::<Vec<&str>>();

        let msg_type = decoded_msg.get(0).unwrap().to_string();
        let json_msg = decoded_msg.get(1).unwrap_or(&"").to_string();

        // JDW.PLAY.MIDI::{"target": "fs001", "tone": 44, "sus_ms": 200.0, "amp": 1.0}
        if msg_type == "JDW.PLAY.MIDI" {
            let data: MIDINotePlayMessage = serde_json::from_str(&json_msg).unwrap();
            if data.amp > 0.0 {
                JDWMIDIServer::play(midi_server.clone(), data);
            }
        }
    }
}