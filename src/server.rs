use std::collections::{BTreeMap, HashMap};
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::sleep;
use std::time;
use std::time::Duration;

use midir::{MidiOutput, MidiOutputConnection};


use crate::midi_lib::*;
use crate::model::MIDINotePlayMessage;

pub struct JDWMIDIServer {
    output_connections: Arc<Mutex<HashMap<String, Arc<Mutex<MidiOutputConnection>>>>>,
    polling: bool
}

impl JDWMIDIServer {
    pub fn new() -> Self {
        JDWMIDIServer {
            output_connections: Arc::new(Mutex::new(HashMap::new())),
            polling: false
        }
    }

    pub fn sync_all(this: Arc<Mutex<JDWMIDIServer>>) {
        for (_, output) in this.lock().unwrap().output_connections.lock().unwrap().iter() {
            output.lock().unwrap().send(&[
                CLOCK_SYNC // TODO: would be "+ index" for a channel, but for now only 0 is used
            ]).ok();
        }
    }

    pub fn play(
        this: Arc<Mutex<JDWMIDIServer>>,
        note: MIDINotePlayMessage,
    ) {
        match this.lock().unwrap()
            .output_connections
            .lock()
            .unwrap()
            .get_mut(&note.target)
        {
            Some(output) => {

                println!("Playing note: {} {}", &note.target, &note.tone);

                output.lock().unwrap().send(&[
                    NOTE_ON_HEX, // TODO: would be "+ index" for a channel, but for now only 0 is used
                    note.tone as u8,
                    midi_velocity(&note.amp)
                ]).ok();

                ()
            }
            None => {
                println!("ERROR: No such output: {}", &note.target);
            }
        };

        thread::spawn(move || {

            let sleep_time =
                time::Duration::from_millis(
                    note.sus_ms as u64
                );

            sleep(sleep_time);

            match this.lock().unwrap()
                .output_connections
                .lock()
                .unwrap()
                .get_mut(&note.target)
            {
                Some(output) => {
                    output.lock().unwrap().send(&[
                        NOTE_OFF_HEX, // TODO: would be "+ index" for a channel, but for now only 0 is used
                        note.tone as u8,
                        midi_velocity(&note.amp)
                    ]).ok();

                    ()
                }
                None => {} // Unimportant for now
            };

        });
    }

    pub fn poll_outputs(this: Arc<Mutex<JDWMIDIServer>>) {

        // Make sure not to start duplicate threads
        {
            let mut self_lock = this.lock().unwrap();
            if self_lock.polling {
                return ();
            } else {
                println!("Began polling for outputs...");
                self_lock.polling = true;
            }
        }

        thread::spawn(move || {
            loop {
                this.lock().unwrap().connect_outputs();
                sleep(Duration::from_secs(2));
            }
        });

    }

    /*
        Scan for all existing MIDI outputs and create named connections to them.
        Should be repeatable; already existing outputs are ignored.
     */
    pub fn connect_outputs(&mut self) {
        let scanner = MidiOutput::new("JDW_SCAN").unwrap();
        let mut sorted_port_map: BTreeMap<String, usize> = BTreeMap::new();

        for i in 0..scanner.port_count() {
            sorted_port_map.insert(scanner.port_name(i).unwrap(), i);
        }

        for (name, index) in sorted_port_map {
            let assigned_name: String = if name.contains(":") {
                let colon_index = name.rfind(":").unwrap();

                // Strip whitespace and colon to infer a viable name
                // Has to do with how inputs are sometimes listed as <source>: <output>
                // which is both verbose and error-prone
                let mut striped = String::from(&name[colon_index+1..]);
                striped.retain(|c| {!c.is_whitespace()});
                striped

            } else {
                name.clone()
            };

            let exists = self.output_connections
                .lock()
                .unwrap()
                .contains_key(&assigned_name);

            if !exists {

                println!("Added new MIDI output with key: {}", assigned_name);

                let output_client = MidiOutput::new(&assigned_name).unwrap();
                let output_connection = output_client
                    .connect(index, &assigned_name)
                    .unwrap();

                self.output_connections.lock().unwrap().insert(
                    assigned_name.to_string(),
                    // Wrap it in a nice thread-safe ribbon
                    Arc::new(Mutex::new(output_connection)),
                );
            }

        }
    }
}