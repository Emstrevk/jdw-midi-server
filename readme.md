# Simple midi output server
- Polls for available MIDI outputs and assigns them a name-based id
- Subscribes to jdw-broker messages to trigger MIDI events (notes, sync, etc.)
